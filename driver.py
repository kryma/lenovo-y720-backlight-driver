#!/usr/bin/env python3
import argparse
from enum import Enum
from fcntl import ioctl
import os
from threading import Lock

class Style(Enum):
    HEARTBEAT = 0
    BREATH = 1
    SMOOTH = 2
    ALWAYS_ON = 3
    WAVE = 4

class Colour(Enum):
    CRIMSON = 0
    TORCH_RED = 1
    HOLLYWOOD_CERISE = 2
    MAGENTA = 3
    ELECTRIC_VIOLET = 4
    ELECTRIC_VIOLET_2 = 5
    BLUE = 6
    BLUE_RIBBON = 7
    AZURE_RADIANCE = 8
    CYAN = 9
    SPRING_GREEN = 10
    SPRING_GREEN_2 = 11
    GREEN = 12
    BRIGHT_GREEN = 13
    LIME = 14
    YELLOW = 15
    WEB_ORANGE = 16
    INTERNATIONAL_ORANGE = 17
    WHITE = 18
    NO_COLOR = 19
    
file_lock = Lock()

def set_status(style, cols, brightness=[3, 3, 3, 3]):
    if not isinstance(style, int):
        if isinstance(style, Style):
            style = style.value
        else:
            raise ValueError(f"Expected style to be enum or int. You give: {style}")
    if not isinstance(cols, tuple) and not isinstance(cols, list):
        raise ValueError(f"Expected cols to be tuple or list. You give: {cols}")
    if len(cols) != 4:
        raise ValueError(f"Expected cols to be of length 4. You give: {cols}")
    for i in range(len(cols)):
        if not isinstance(cols[i], int):
            if isinstance(cols[i], Colour):
                cols[i] = cols[i].value
            else:
                raise ValueError(f"Expected cols[{i}] to be enum or int. You give: {cols[i]}")
    if not isinstance(brightness, tuple) and not isinstance(brightness, list):
        raise ValueError(f"Expected brightness to be tuple or list. You give: {brightness}")
    if len(brightness) != 4:
        raise ValueError(f"Expected brightness to be of length 4. You give: {brightness}")
    for i in range(len(brightness)):
        if not isinstance(brightness[i], int):
            raise ValueError(f"Expected brightness[{i}] to be int. You give: {brightness[i]}")
    for i in os.listdir("/sys/class/hidraw/"):
        with open(f"/sys/class/hidraw/{i}/device/uevent", "r") as classfile:
            if classfile.readlines()[2] == "HID_NAME=ITE33D1:00 048D:837A\n":
                file_lock.acquire()
                fd = os.open(f"/dev/{i}", os.O_WRONLY)
                for i in range(len(cols)):
                    string = bytearray(b"\xCC\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
                    string[2] = style
                    string[3] = cols[i]
                    string[4] = brightness[i]
                    string[5] = i
                    ioctl(fd, 0xC0114806, string)
                    ioctl(fd, 0xC0114806, b"\xCC\x09\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
                os.close(fd)
                file_lock.release()


def get_status():
    ret = dict()
    ret["cols"] = [0, 0, 0, 0]
    ret["brightness"] = [0, 0, 0, 0]
    for i in os.listdir("/sys/class/hidraw/"):
        with open(f"/sys/class/hidraw/{i}/device/uevent", "r") as classfile:
            if classfile.readlines()[2] == "HID_NAME=ITE33D1:00 048D:837A\n":
                file_lock.acquire()
                fd = os.open(f"/dev/{i}", os.O_WRONLY)
                for i in range(4):
                    string = bytearray(b"\xCC\x80\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00")
                    string[5] = i
                    ioctl(fd, 0xC0114806, string)
                    ioctl(fd, 0xC0114807, string)
                    ret["style"] = Style(string[2])
                    ret["cols"][i] = Colour(string[3])
                    ret["brightness"][i] = string[4]
                os.close(fd)
                file_lock.release()
    return ret


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("style", metavar="style", type=int, nargs="?",
                        default=Style.ALWAYS_ON, help="Backlight style. ")
    parser.add_argument("cols", metavar="colour", type=int, nargs="*",
                        default=[Colour.CRIMSON for i in range(4)], help="Colours of the keyboard sectors")
    args = parser.parse_args()
    set_status(args.style, args.cols)
